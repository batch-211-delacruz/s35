const express = require("express");
// Mongoose is a package that allows creation of Schemas to model our data structures
// also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// Connect to the database by passing in our connection string, remember to replace the password and data base names with actual values
// {newUrlParser:true} allows us to avoid any current and future errors while connecting to MongoDB
// Syntax
	// mongoose.connect("<MongoDB connection string>",{useNewUrlParse:true})

// Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://admin123:admin123@project0.98ltevx.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
	}
);

//Se notificzations for connection success or failure
//connection to the database
//allows us to handle errors when the initital connection is established
//works with on and once Mongoose methods
let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
//if a connection error occured, out put in the console
//console.error.bind(console) allows us to print errors in our terminal

db.once("open",() => console.log("We're connected to the cloud database"))

// Mongoose Schemas
// Schemas determine the structure of the documents to be writen in the database
// Schemas act as blueprints to our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object
//the "new" keyword created a new Schema
const taskSchema = new mongoose.Schema({
	// define the fields with their corresponding data type

	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

//Models
//uses schemas and are used to create/instantiate objects that correspond to the schema
//Server>Schema(blueprintt)>Database>Collection
// models must be single word and capitalized
const Task = mongoose.model("Task",taskSchema)

// Setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Create a new task
// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		-if the task already exists in the database, we return an error
		-if the task does not exist in the database, we add it in the database
	2. The task data will be coming from the requests's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaulty it to "pending" upon creation of an object
*/
app.post("/signup",(req,res) =>{

	Task.findOne({name:req.body.name},(err,result)=>{
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found")
		}else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr,savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}else {
					return res.status(201).send("New task created");
				}
			})
		}
	})
})


app.get("/tasks",(req,res)=>{
	Task.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}else {
			return res.status(200).json({
				data : result
			})
		}
	})
})


const userSchema = new mongoose.Schema({


	name: String,
	password: Number
})
const User = mongoose.model("User",userSchema)
// Activity
app.post("/users",(req,res)=>{

	User.findOne({name:req.body.name, password:req.body.password},(err,result)=>{
		if(result != null && result.name == req.body.name){
			return res.send("Error already exists")
		}else {
			let newUser = new User({
				name: req.body.name,
				password: req.body.password
			});
			newUser.save((saveErr,savedUser) =>{
				if(saveErr){
					return console.error(saveErr);
				}else {
					return res.status(201).send("New user registered")
				}
			})
		}
	})
})


app.get("/allusers",(req,res)=>{
	User.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}else {
			return res.status(200).json({
				data : result
			})
		}
	})
})

app.listen(port,() => console.log(`Server running at port ${port}`))

